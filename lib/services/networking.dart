import 'dart:convert';

// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;

class NetworkHelper {
  NetworkHelper(this.url);
  final String url;
  List rootList = [];

  Future getData() async {
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      jsonDecode(response.body).forEach((k, v) {
        rootList.add({'name': k, 'results': v});
      });

      return rootList;
    } else {
      print(response.statusCode);
    }
  }
}
